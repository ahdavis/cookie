/*
 * Constants.ts
 * Defines constants for cookie
 * Created on 5/10/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * Manages named constants
 */
export class Constants {
	/**
	 * The side length (in screen pixels)
	 * of a given emulator pixel
	 */
	public static readonly PIXEL_DIM: number = 16;

	/**
	 * The number of emulator pixels
	 * along a horizontal side of the
	 * emulator window
	 */
	public static readonly WIN_WIDTH: number = 64;

	/**
	 * The number of emulator pixels
	 * along a vertical side of the
	 * emulator window
	 */
	public static readonly WIN_HEIGHT: number = 32;
}

//end of file
