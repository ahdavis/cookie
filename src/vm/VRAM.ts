/*
 * VRAM.ts
 * Defines a class that manages virtual memory
 * Created on 5/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { Sprite } from '../gfx/Sprite';
import { Program } from '../util/Program';

/**
 * Manages virtual memory
 */
export class VRAM {
	//constants
	/**
	 * The memory address of the start of font ROM
	 */
	public static readonly FNT_START: number = 0x050;

	/**
	 * The memory address of the end of font ROM
	 */
	public static readonly FNT_END: number = 0x0A0;

	/**
	 * The memory address of the start of program RAM
	 */
	public static readonly PROG_START: number = 0x200;

	/**
	 * The number of total bytes available to the emulator
	 */
	public static readonly MEM_SIZE: number = 0x1000;

	//field
	/**
	 * The actual memory array
	 */
	private _data: number[];

	//methods

	/**
	 * Constructs a new VRAM instance
	 */
	constructor() {
		//allocate the data
		this._data = new Array<number>();

		//load it with zeroes
		for(let i = 0; i < VRAM.MEM_SIZE; i++) {
			this._data.push(0x00);
		}

		//and initialize font ROM
		this.initFont();
	}

	/**
	 * Returns the byte at a given memory address
	 *
	 * @param addr The address to read from
	 *
	 * @returns The byte at the given address
	 */
	public peekByte(addr: number): number {
		return this._data[addr & 0x0FFF] & 0xFF;
	}

	/**
	 * Sets the byte at a given memory address
	 *
	 * @param val The byte to set
	 * @param addr The address to modify
	 */
	public pokeByte(val: number, addr: number): void {
		this._data[addr & 0x0FFF] = val & 0xFF;
	}

	/**
	 * Gets the word at a given memory address
	 *
	 * @param addr The address to read from
	 *
	 * @returns The word at the given address
	 */
	public peekWord(addr: number): number {
		//get the individual bytes
		let lsb = this.peekByte(addr);
		let msb = this.peekByte(addr + 1);

		//merge them
		let ret = (lsb << 8 | msb) & 0xFFFF;

		//and return the result
		return ret;
	}

	/**
	 * Sets the word at a given memory address
	 *
	 * @param word The word to set
	 * @param addr The address to modify
	 */
	public pokeWord(word: number, addr: number): void {
		//mask the word to simulate a 16-bit value
		let maskedWord = word & 0xFFFF;

		//split the masked word into two bytes
		let lsb = maskedWord & 0xFF;
		let msb = maskedWord >> 8;

		//and store the bytes into memory
		this.pokeByte(lsb, addr);
		this.pokeByte(msb, addr + 1);
	}

	/**
	 * Retrieves a [[Sprite]] from RAM
	 * with a given height
	 *
	 * @param addr The address of the first byte of the sprite
	 * @param height The height of the sprite (in lines)
	 *
	 * @returns The sprite described by the arguments
	 */
	public getSprite(addr: number, height: number): Sprite {
		//create an array to hold the sprite data
		let sprData = new Array<number>();

		//loop and get the sprite data
		for(let i = 0; i < height; i++) {
			sprData.push(this.peekByte(addr + i));
		}

		//and return the retrieved sprite
		return new Sprite(sprData);
	}

	/**
	 * Loads a binary [[Program]] into memory
	 *
	 * @param prog The program to load
	 *
	 * @returns Whether the loading succeeded
	 */
	public loadProgram(prog: Program): boolean {
		//make sure the program is not too long
		if((prog.length + VRAM.PROG_START) >= VRAM.MEM_SIZE) {
			return false;
		}

		//load the program
		for(let i = 0; i < prog.length; i++) {
			this.pokeByte(prog.code[i], i + VRAM.PROG_START);
		}

		//and return a success
		return true;
	}

	/**
	 * Initializes font ROM
	 */
	private initFont(): void {
		//create an array of the font symbols
		let font = [
			0xF0, 0x90, 0x90, 0x90, 0xF0, //0
			0x20, 0x60, 0x20, 0x20, 0x70, //1
			0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
			0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
			0x90, 0x90, 0xF0, 0x10, 0x10, //4
			0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
			0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
			0xF0, 0x10, 0x20, 0x40, 0x40, //7
			0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
			0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
			0xF0, 0x90, 0xF0, 0x90, 0x90, //A
			0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
			0xF0, 0x80, 0x80, 0x80, 0xF0, //C
			0xE0, 0x90, 0x90, 0x90, 0xE0, //D
			0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
			0xF0, 0x80, 0xF0, 0x80, 0x80  //F
		];

		//loop and load the fontset into memory
		for(let i = 0; i < 80; i++) {
			this.pokeByte(font[i], i + VRAM.FNT_START);
		}
	}
}

//end of file
