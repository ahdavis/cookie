/*
 * Main.ts
 * Main class for cookie
 * Created on 5/10/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { BrowserWindow } from 'electron';
import { Constants } from './util/Constants';
import * as url from 'url';
import * as path from 'path';

/**
 * The main class for the app
 */
export default class Main {
	//fields
	/**
	 * The main window for the app
	 */
	private static mainWindow: Electron.BrowserWindow;

	/**
	 * The Electron application framework
	 */
	private static application: Electron.App;

	/**
	 * The type for the window
	 */
	private static BrowserWindow;

	//methods

	/**
	 * Main entry point for the app
	 *
	 * @param app The application framework
	 * @param browserWindow The window type for the app
	 */
	public static main(app: Electron.App, 
			browserWindow: typeof BrowserWindow): void {
		//init the fields
		Main.BrowserWindow = browserWindow;
		Main.application = app;

		//and setup event listeners
		Main.application.on('window-all-closed',
					Main.onWindowAllClosed);
		Main.application.on('ready', Main.onReady);
	}

	/**
	 * Called when all app windows are closed
	 */
	private static onWindowAllClosed(): void {
		if(process.platform !== 'darwin') {
			Main.application.quit();
		}
	}

	/**
	 * Called when the window is closed
	 */
	private static onClose(): void {
		//Dereference the window object
		Main.mainWindow = null;
	}

	/**
	 * Called when the app is ready
	 */
	private static onReady(): void {
		//create the window
		Main.mainWindow = new Main.BrowserWindow({
					width: Constants.WIN_WIDTH *
						Constants.PIXEL_DIM,
					height: (Constants.WIN_HEIGHT *
						Constants.PIXEL_DIM) + 25
					});

		//load the HTML file
		Main.mainWindow.loadURL(
			url.format({
				pathname: path.join(__dirname, 
							'index.html'),
				protocol: 'file:',
				slashes: true
			})
		);

		//and add the close event handler
		Main.mainWindow.on('closed', Main.onClose);
	}
}

//end of file
