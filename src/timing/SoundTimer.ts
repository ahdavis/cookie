/*
 * SoundTimer.ts
 * Defines a class that represents a sound timer
 * Created on 5/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//import
import { Timer } from './Timer';

/**
 * A sound timer
 */
export class SoundTimer extends Timer {
	//field
	/**
	 * The audio context for playing sounds
	 */
	private _sndCtxt: AudioContext;

	//methods

	/**
	 * Constructs a SoundTimer instance
	 */
	constructor() {
		super();
		this._sndCtxt = new AudioContext();
	}

	/**
	 * Called when the timer ticks
	 */
	protected onTick(): void {
		//check for the end of the timer
		if(this._ticks === 0) {
			window.clearInterval(this._interval);
			this._interval = null;
		} else { //end not reached
			this._ticks--;
			this.beep();
		}
	}


	/**
	 * Plays a short beeping sound
	 */
	private beep(): void {
		//create an oscillator
		let osc = this._sndCtxt.createOscillator();

		//create gain
		let gain = this._sndCtxt.createGain();

		//connect the oscillator to the gain
		osc.connect(gain);

		//set the frequency
		osc.frequency.value = 520;

		//set the type
		osc.type = 'square';

		//connect the gain to the context
		gain.connect(this._sndCtxt.destination);

		//set the volume
		gain.gain.value = 0.5;

		//start the sound
		osc.start(this._sndCtxt.currentTime);

		//and stop the sound
		osc.stop(this._sndCtxt.currentTime + 0.2);
	}
}

//end of file
