/*
 * Keycode.ts
 * Enumerates input keycodes
 * Created on 5/19/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * An input keycode
 */
export enum Keycode {
	K1 = 0x0, //numeric 1 key
	K2 = 0x1, //numeric 2 key
	K3 = 0x2, //numeric 3 key
	K4 = 0x3, //numeric 4 key
	KQ = 0x4, //Q key
	KW = 0x5, //W key
	KE = 0x6, //E key
	KR = 0x7, //R key
	KA = 0x8, //A key
	KS = 0x9, //S key
	KD = 0xA, //D key
	KF = 0xB, //F key
	KZ = 0xC, //Z key
	KX = 0xD, //X key
	KC = 0xE, //C key
	KV = 0xF, //V key
	KNULL = 0x10 //null key
}

//end of file
