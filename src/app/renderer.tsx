/*
 * renderer.tsx
 * Renderer entry point for cookie
 * Created on 5/10/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import "@babel/polyfill";
import { Renderer } from '../gfx/Renderer';
import { VRAM } from '../vm/VRAM';
import { Sprite } from '../gfx/Sprite';
import { KeyManager } from '../input/KeyManager';
import { Keycode } from '../input/Keycode';

//create a renderer
let rend = new Renderer();

//create VRAM
let mem = new VRAM();

//create a key manager
let mgr = new KeyManager();

//draw a blank screen
rend.clearScreen();
rend.update();

//await keypresses and draw the input sprites
(async function() {
	while(true) {
		let kc = await mgr.getKey();
		let spr = mem.getSprite((5 * kc) + VRAM.FNT_START, 5);
		rend.clearScreen();
		rend.drawSprite(spr, 13, 27);
		rend.update();
	}
}());

//end of file
