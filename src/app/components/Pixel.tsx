/*
 * Pixel.tsx
 * Emulator pixel component for Cookie
 * Created on 5/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { PixelState } from '../../gfx/PixelState';
import { PixelStates } from '../../states/PixelStates';
import { PixelProps } from '../../props/PixelProps';
import { Constants } from '../../util/Constants';

/**
 * A single emulator pixel
 */
export class Pixel extends React.Component<PixelProps, PixelStates> {
	/**
	 * Constructs a new Pixel instance
	 *
	 * @param props The properties of the Pixel
	 */
	constructor(props: PixelProps) {
		super(props); //call the superclass constructor
	}

	/**
	 * Renders the Pixel
	 *
	 * @returns The React DOM for the Pixel
	 */
	public render(): React.ReactNode {
		return (
			<td style={this.composeTDStyling()}>
				<div style={this.composeDivStyling()}>
				</div>
			</td>
		);
	}

	/**
	 * Returns the styling for the Pixel div
	 *
	 * @returns The CSS styling for the Pixel div
	 */
	private composeDivStyling(): React.CSSProperties {
		//calculate the background color string
		let bgCol: string;
		if(this.props.state == PixelState.ON) {
			bgCol = '#FFFFFF';
		} else {
			bgCol = '#000000';
		}

		//get the width and height components
		let pWidth = ((1.0 / Constants.WIN_HEIGHT) * 100) + '%';
		let pHeight = ((1.0 / Constants.WIN_WIDTH) * 100) + '%';

		//and assemble the styling object
		return {
			width: Constants.PIXEL_DIM + 'px',
			height: Constants.PIXEL_DIM + 'px',
			backgroundColor: bgCol 
		};
	}

	/**
	 * Returns the styling for the table data element
	 *
	 * @returns The styling for the table data element
	 */
	private composeTDStyling(): React.CSSProperties {
		return {
			paddingTop: '0',
			paddingBottom: '0',
			paddingLeft: '0',
			paddingRight: '0'
		};
	}
}

//end of file
