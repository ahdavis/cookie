/*
 * Window.tsx
 * Main window component for Cookie
 * Created on 5/10/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as React from 'react';
import { WindowProps } from '../../props/WindowProps';
import { WindowState } from '../../states/WindowState';
import { PixelRow } from './PixelRow';
import { Constants } from '../../util/Constants';

/**
 * The main window component
 */
export class Window extends React.Component<WindowProps, WindowState> {
	/**
	 * Constructs a new Window component
	 *
	 * @param props The properties of the Window
	 */
	constructor(props: WindowProps) {
		super(props); //call the superclass constructor
	}

	/**
	 * Renders the Window
	 *
	 * @returns A React DOM representing the Window
	 */
	public render(): React.ReactNode {
		return (
			<table style={this.styling()}>
				<tbody>{this.assembleDOM()}</tbody>
			</table>
		);
	}

	/**
	 * Assembles the React DOM for the Window
	 *
	 * @returns The React DOM for the Window
	 */
	private assembleDOM(): React.ReactNode {
		//declare the return value
		let ret: React.ReactNode[] = [];

		//loop and populate it
		for(let i = 0; i < Constants.WIN_HEIGHT; i++) {
			ret.push(<PixelRow frame={this.props.frame}
					index={i} key={'row ' + i}/>);
		}

		//and return it
		return ret;
	}

	/**
	 * Assembles the styling for the Window
	 *
	 * @returns the styling for the Window
	 */
	private styling(): React.CSSProperties {
		//calculate the width and height
		let tWidth = Constants.WIN_WIDTH * Constants.PIXEL_DIM;
		let tHeight = Constants.WIN_HEIGHT * Constants.PIXEL_DIM;

		return {
			borderCollapse: 'collapse',
			top: '0',
			bottom: '0',
			left: '0',
			right: '0',
			position: 'absolute',
			width: tWidth + 'px',
			height: tHeight + 'px'
		};
	}
}

//end of file
