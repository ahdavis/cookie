/*
 * FrameBuffer.ts
 * Defines a class that represents a frame buffer
 * Created on 5/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import { PixelState } from './PixelState';
import { Constants } from '../util/Constants';

/**
 * A single rendered frame
 */
export class FrameBuffer {
	//field
	/**
	 * The pixels that make up the frame
	 */
	private _pixels: PixelState[][];

	//methods

	/**
	 * Constructs a new FrameBuffer instance
	 */
	constructor() {
		//allocate the row arrays
		this._pixels = new Array<Array<PixelState>>(
						Constants.WIN_HEIGHT);

		//allocate the column arrays
		for(let y = 0; y < Constants.WIN_HEIGHT; y++) {
			this._pixels[y] = new Array<PixelState>(
						Constants.WIN_HEIGHT);
		}

		//and initialize the entire array
		for(let x = 0; x < Constants.WIN_WIDTH; x++) {
			for(let y = 0; y < Constants.WIN_HEIGHT; y++) {
				this._pixels[y][x] = PixelState.OFF;
			}
		}
	}

	/**
	 * Returns the state of the pixel at given x and y coordinates
	 *
	 * @param row The x-coordinate to check the state at
	 * @param col The y-coordinate to check the state at
	 *
	 * @returns The state of the pixel at the given coordinates
	 */
	public stateAtCoords(row: number, col: number): PixelState {
		//validate the coordinates
		if((col < 0) || (col >= Constants.WIN_WIDTH)) {
			return PixelState.OFF;
		}
		if((row < 0) || (row >= Constants.WIN_HEIGHT)) {
			return PixelState.OFF;
		}

		//and return the state
		return this._pixels[row][col];
	}

	/**
	 * Toggles the pixel at given coordinates
	 *
	 * @param row The x-coord of the pixel to toggle
	 * @param col The y-coord of the pixel to toggle
	 *
	 * @returns Whether the pixel was toggled from on to off
	 */
	public toggleAtCoords(row: number, col: number): boolean {
		//validate the coordinates
		if((col < 0) || (col >= Constants.WIN_WIDTH)) {
			return false;
		}
		if((row < 0) || (row >= Constants.WIN_HEIGHT)) {
			return false;
		}

		//and toggle the pixel
		if(this._pixels[row][col] === PixelState.OFF) {
			this._pixels[row][col] = PixelState.ON;
			return false;
		} else {
			this._pixels[row][col] = PixelState.OFF;
			return true;
		}
	}

	/**
	 * Clears the frame
	 */
	public clear(): void {
		//loop and clear each pixel
		for(let x = 0; x < Constants.WIN_WIDTH; x++) {
			for(let y = 0; y < Constants.WIN_HEIGHT; y++) {
				this._pixels[y][x] = PixelState.OFF;
			}
		}
	}
}

//end of file
