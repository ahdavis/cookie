/*
 * Sprite.ts
 * Defines a class that represents a sprite
 * Created on 5/20/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//no imports

/**
 * A graphical sprite
 */
export class Sprite {
	//fields
	/**
	 * The bytes that make up the sprite
	 */
	private _bytes: number[];

	/**
	 * The height of the sprite (in pixels)
	 */
	private _height: number;

	//methods

	/**
	 * Constructs a new Sprite instance
	 *
	 * @param bytes The binary sprite data
	 */
	constructor(bytes: number[]) {
		this._bytes = new Array<number>(); //allocate the bytes

		//loop and copy the data
		for(let i = 0; i < bytes.length; i++) {
			this._bytes.push(bytes[i] & 0x000000FF);
		}

		//and initialize the height field
		this._height = bytes.length;
	}

	/**
	 * Gets the height of the sprite
	 *
	 * @returns The height of the sprite
	 */
	public get height(): number {
		return this._height;
	}

	/**
	 * Returns whether the bit at a given row and column
	 * in the sprite is set
	 *
	 * @param row The row to check at
	 * @param col The column to check at
	 *
	 * @returns Whether the specified bit is set
	 */
	public hasBitAt(row: number, col: number): boolean {
		//get the byte corresponding to the row
		let curByte = this._bytes[row];

		//flip the column around
		//so it indexes from the right side,
		//making the math easier
		let colComp = 8 - col;

		//calculate whether the bit
		//corresponding to the column
		//is set
		let ret = ((curByte & (1 << colComp)) !== 0);

		//and return the result
		return ret;
	}
}

//end of file
