/*
 * Renderer.tsx
 * Defines a class that represents a graphics renderer
 * Created on 5/18/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//imports
import * as ReactDOM from 'react-dom';
import * as React from 'react';
import { Window } from '../app/components/Window';
import { FrameBuffer } from './FrameBuffer';
import { PixelState } from './PixelState';
import { Sprite } from './Sprite';

/**
 * A graphics renderer
 */
export class Renderer {
	//fields
	/**
	 * The current frame being drawn
	 */
	private _curFrame: FrameBuffer;

	/**
	 * Has the rendering target been updated?
	 */
	private _wasUpdated: boolean;

	//methods

	/**
	 * Constructs a new Renderer instance
	 */
	constructor() {
		this._curFrame = new FrameBuffer();
		this._wasUpdated = false;
	}

	/**
	 * Toggles the pixel at given coordinates on the screen
	 *
	 * @param row The row the target pixel is on
	 * @param col The column the target pixel is on
	 *
	 * @returns Whether the pixel was on and was then turned off
	 */
	public toggleAtCoords(row: number, col: number): boolean {
		this._wasUpdated = true;
		return this._curFrame.toggleAtCoords(row, col);
	}

	/**
	 * Draws a sprite to the screen at given coordinates
	 *
	 * @param spr The sprite to draw
	 * @param row The row to draw the sprite to
	 * @param col The column to draw the sprite to
	 *
	 * @returns Whether any pixels were turned from on to off
	 */
	public drawSprite(spr: Sprite, row: number, col: number): boolean {
		//declare the return value
		let ret = false;

		//loop and draw the sprite
		for(let xLine = 0; xLine < 8; xLine++) {
			for(let yLine = 0; yLine < spr.height; yLine++) {
				if(spr.hasBitAt(yLine, xLine)) {
					if(this.toggleAtCoords(
						row + yLine,
						col + xLine)) {
						ret = true;
					}
				}
			}
		}

		//and return the return value
		return ret;
	}

	/**
	 * Clears the screen
	 */
	public clearScreen(): void {
		this._wasUpdated = true;
		this._curFrame.clear();
	}

	/**
	 * Updates the rendering target
	 */
	public update(): void {
		//make sure that the target was updated
		if(!this._wasUpdated) {
			return;
		}

		//render the window
		ReactDOM.render(<Window frame={this._curFrame} />,
				document.getElementById('renderer'));

		//and clear the update flag
		this._wasUpdated = false;
	}
}

//end of file
